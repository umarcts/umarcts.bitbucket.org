# ARC-TS Targetprocess Training
Author: msbritt   
Date: March 2018   



## Scrum Process Review

Scrum is a management framework for incremental product development using one or more cross-functional, self-organizing teams.  Through iteration and feedback, this framework can help create products that most closes matches the needs of the product owner and end user.



## Why Targetprocess 

Targetprocess lets us manage our project-based workloads using either a
Scrum or Kanban in a visual way, being able to manage a single project up to giving an overall view at what ARC-TS is working on as a whole.



## Targetprocess Overview

Targetprocess is the cloud-based tool ARC-TS uses to manage project and
critical work. Below are the current features and definitions of
Targetprocess ARC-TS is currently using.


### Projects
***Projects:*** A collection or set of work that needs to be completed for a common goal. At this level, it is most often a product or project. According to Targetprocess, it is usually created as a high level idea first and managed as a portfolio item later.

### Epics
***Epics:*** Not currently being used here.  Part of a project but is something too large to fit into a single release. An epic can be used to represent a large piece of functionality or to simply group features by some characteristic.

### Features

***Features:*** Large pieces of work that in the Iterative methodology world could also be called ‘Themes’. Typically comprised of multiple User Stories. Normally something too large to be implemented during a single iteration but is small enough to fit in a release. Features are also most often some sort of distinct item in itself that a customer will appreciate. It also may be considered a business asset or can be reused with other efforts. Think of a distinct characteristic that contributes to the overall design and functionality of the final product. *Features usually tend to describe what your product will do.*


### User Stories

***User Stories:*** A distinct piece of customer visible functionality. They can be planned for a release, iteration, or team iteration. Where this differs from a Feature is that features are more specific and sometimes tell how a something should work in order to fulfill the customer requirements. *The stories are the requirements. User stories tend to express what the user wants to do.*

### Tasks

**Tasks:** Small, atomic actions that can be taken to complete a Story. Often there is more than one task within a story. These are things small enough to be assigned to and completed by one individual.



### Bugs

**Bugs:** Something that is a defect or isn’t working as intended 


### Relations

**Relations, blockers, and dependencies:** These are most often items that are preventing a story or task from moving forward. OR - vice versa, you are in a dependent story that needs something else to complete before progress can be made. Occasionally people will use relations to identify tasks that are in progress together or have some sort of mutual goal or benefit from their completion together. 
 



### Requests

**Requests:** Got a great idea for a new service or using a new tool? Cool. Put in a Request. At some point in the future, we are talking about implementing a Rating capability to allow people to vote 


### Example for Epic, Feature, & User Story Relationship: Single Sign On
**Request**:  Someone from the team requests not having a separate password to log into Targetprocess and would like to use UMich credentials.
**Project**: Targetprocess
**Epic**: [reminder: we're not using epics yet]. Allow the customer to be able to sign in with umich credentials   
**Feature**: Enable Shibboleth for Targetprocess
**User Story**: As an end user, I want to be able to login with my umich account so I do not have to manage yet another account password.
**Task**: Insert certificate into Targetprocss SSO configuration and test
**Blocker**:  Previous task is blocked on getting URL string from ITS
 

### State 

**State** (Open, Planned, In Progress, Done)
Open is typically the default state when an item is ‘Opened’. If an item is not a fully approved one or still doesn’t have enough information, it can remain in the backlog with an Open state. Once the item is recognized as a story that will definitely be completed, it can be moved to a Planned state in the backlog. Ideally, all stories added to the sprint backlog have been in the Planned state and it aligns to your overall roadmap and release plan. Once someone in the team grabs an item to begin working on it, the state should change to In Progress. Some teams are using QA and in those cases, an In Progress item could change state to QA if a task has been handed off to someone else for testing or if someone is simply finished with any development but is currently testing the results. A Done state is self explanatory.   



### Default Workflow
From the Sprint board, all items will automatically change state if you drag and drop a card in the corresponding vertical swim lane. (Open, Planned, In Progress, QA, Done)
In Progress is automatically set on an item when an individual has been assigned to it. 
Workflows can be customized by the Project and/or by the Team. We are currently using the defaults.


## Common Targetprocess Elements   

Targetprocess offers several features to view our project work. From
filters, focus, and views, you can access and view the data in many different ways.  


## Views

Targetprocess gives you multiple views to look at the same data, some of
which have different capabilities of working with that data (e.g. it can
be easier to sort/rank items).


### Pre-Made Views
There a several pre-canned views provided by Targetprocess.

<img src="images/targetprocess_pre-canned_views.png" width="80%">


### Favoriting a View
<img src="images/targetprocess_favorite_views.png" width="80%">
### Sharing Views

### View Types 

#### Board View (Kanban-esque.  Cards w/ vertical and horizontal lanes)


#### Detailed view


#### List view


#### Timeline View

## Filters

## Sorting

## Search

## Action Button/Menu

## Focus Button

## Common Tasks

### General 

#### Project View

###

### Sprint Tasks View

### Teams


### Test screen
Testing

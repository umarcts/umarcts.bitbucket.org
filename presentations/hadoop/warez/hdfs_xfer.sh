# Get the sample data used later in the presentation
git clone https://bitbucket.org/umarcts/presentations.git
cd presentations

# Upload sample data for later use
hdfs dfs -put hadoop/ngrams.data .
hdfs dfs -put hadoop/ngrams.data ngrams.hive

# Download some data
hdfs dfs -get ngrams.hive .
